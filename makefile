PRG=a.exe
GCC=g++
GCCFLAGS=-O2 -Wall -Wextra -std=c++11 -pedantic -Weffc++ -Wold-style-cast -Woverloaded-virtual -Wsign-promo  -Wctor-dtor-privacy -Wnon-virtual-dtor -Wreorder
GCCFLAGS_DEBUG=-O0 -g -Wall -Wold-style-cast -Wextra -std=c++11 -pedantic -Wconversion -Wno-unused -DLOUIS_DEBUG
OBJECTS0=closestpair.cpp
DRIVER0=driver.cpp
DRIVER1=my_driver.cpp

VALGRIND_OPTIONS=-q --leak-check=full
DRMEM_GCCFLAGS=-g -fno-inline -fno-omit-frame-pointer -std=c++11
DIFF_OPTIONS=-y --strip-trailing-cr --suppress-common-lines -b

OSTYPE := $(shell uname)
ifeq (,$(findstring CYGWIN,$(OSTYPE)))
CYGWIN=
else
CYGWIN=-Wl,--enable-auto-import
endif

gcc0:
	$(GCC) -o $(PRG) $(CYGWIN) $(DRIVER0) $(OBJECTS0) $(GCCFLAGS)
gcc1:
	$(GCC) -o $(PRG) $(CYGWIN) $(DRIVER0) $(OBJECTS0) $(GCCFLAGS)
dbg0:
	$(GCC) -o $(PRG) $(CYGWIN) $(DRIVER0) $(OBJECTS0) $(GCCFLAGS_DEBUG)
dbg1:
	$(GCC) -o $(PRG) $(CYGWIN) $(DRIVER1) $(OBJECTS0) $(GCCFLAGS_DEBUG)
drmem:
	$(GCC) -o $@.exe $(DRIVER0) $(OBJECTS0) $(DRMEM_GCCFLAGS)
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30:
	@echo "should run in less than 500 ms"
	./$(PRG) $@ >studentout$@
	@echo "lines after the next are mismatches with master output -- see out$@"
	diff out$@ studentout$@ $(DIFF_OPTIONS)
mem0 mem1 mem2 mem3 mem4 mem5 mem6 mem7 mem8 mem9 mem10 mem11 mem12 mem13 mem14 mem15 mem16 mem18 mem19 mem20 mem21 mem22 mem23 mem24 mem25 mem26 mem27 mem28 mem29 mem30:
	@echo "running memory test $@"
	@echo "should run in less than 2500 ms"
	valgrind $(VALGRIND_OPTIONS) ./$(PRG) $(subst mem,,$@) 1>/dev/null 2>difference$@
	@echo "lines after this are memory errors"; cat difference$@
clean :
	rm *.exe *.o
