#include "closestpair.h"
#include <cmath>

#include <sstream>
#include <iostream>
#include <queue>
#include <string>
#include <cassert>
#include <exception>
#include <regex>
#include <fstream>
#include <algorithm>
#include <limits>
#include <iterator>
#include <utility>













// WTFPLv2 licensed DEBUGGING TOOLS for proper asserts in C++
// Only used to color my output for debugging and allowing me to assert with a string
// My program begins on line 1169



















////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

// PPK ASSERTS HEADER FILE
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
// see README.md for usage instructions.
// (‑●‑●)> released under the WTFPL v2 license, by Gregory Pakosz (@gpakosz)

// -- usage --------------------------------------------------------------------
/*

  run-time assertions:

    PPK_ASSERT(expression);
    PPK_ASSERT(expression, message, ...);

    PPK_ASSERT_WARNING(expression);
    PPK_ASSERT_WARNING(expression, message, ...);

    PPK_ASSERT_DEBUG(expression);
    PPK_ASSERT_DEBUG(expression, message, ...);

    PPK_ASSERT_ERROR(expression);
    PPK_ASSERT_ERROR(expression, message);

    PPK_ASSERT_FATAL(expression);
    PPK_ASSERT_FATAL(expression, message, ...);

    PPK_ASSERT_CUSTOM(level, expression);
    PPK_ASSERT_CUSTOM(level, expression, message, ...);

    PPK_ASSERT_USED(type)
    PPK_ASSERT_USED_WARNING(type)
    PPK_ASSERT_USED_DEBUG(type)
    PPK_ASSERT_USED_ERROR(type)
    PPK_ASSERT_USED_FATAL(type)
    PPK_ASSERT_USED_CUSTOM(level, type)

    PPK_ASSERT_USED(bool) foo()
    {
      return true;
    }

  compile-time assertions:

    PPK_STATIC_ASSERT(expression)
    PPK_STATIC_ASSERT(expression, message)

*/

#if !defined(PPK_ASSERT_ENABLED)
#if !defined(NDEBUG)		 // if we are in debug mode
#define PPK_ASSERT_ENABLED 1 // enable them
#endif
#endif

#if !defined(PPK_ASSERT_DEFAULT_LEVEL)
#define PPK_ASSERT_DEFAULT_LEVEL Debug
#endif

// -- implementation -----------------------------------------------------------

#if (defined(__GNUC__) && ((__GNUC__ * 1000 + __GNUC_MINOR__ * 100) >= 4600)) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wvariadic-macros"
#endif

#if defined(__clang__)
#pragma GCC diagnostic ignored "-Wgnu-zero-variadic-macro-arguments"
#endif

#if !defined(PPK_ASSERT_H)
#define PPK_ASSERT_H

#define PPK_ASSERT(...) PPK_ASSERT_(ppk::assert::implementation::AssertLevel::PPK_ASSERT_DEFAULT_LEVEL, __VA_ARGS__)
#define PPK_ASSERT_WARNING(...) PPK_ASSERT_(ppk::assert::implementation::AssertLevel::Warning, __VA_ARGS__)
#define PPK_ASSERT_DEBUG(...) PPK_ASSERT_(ppk::assert::implementation::AssertLevel::Debug, __VA_ARGS__)
#define PPK_ASSERT_ERROR(...) PPK_ASSERT_(ppk::assert::implementation::AssertLevel::Error, __VA_ARGS__)
#define PPK_ASSERT_FATAL(...) PPK_ASSERT_(ppk::assert::implementation::AssertLevel::Fatal, __VA_ARGS__)
#define PPK_ASSERT_CUSTOM(level, ...) PPK_ASSERT_(level, __VA_ARGS__)

#define PPK_ASSERT_USED(...) PPK_ASSERT_USED_(__VA_ARGS__)
#define PPK_ASSERT_USED_WARNING(...) PPK_ASSERT_USED_(ppk::assert::implementation::AssertLevel::Warning, __VA_ARGS__)
#define PPK_ASSERT_USED_DEBUG(...) PPK_ASSERT_USED_(ppk::assert::implementation::AssertLevel::Debug, __VA_ARGS__)
#define PPK_ASSERT_USED_ERROR(...) PPK_ASSERT_USED_(ppk::assert::implementation::AssertLevel::Error, __VA_ARGS__)
#define PPK_ASSERT_USED_FATAL(...) PPK_ASSERT_USED_(ppk::assert::implementation::AssertLevel::Fatal, __VA_ARGS__)
#define PPK_ASSERT_USED_CUSTOM(level, ...) PPK_ASSERT_USED_(level, __VA_ARGS__)

#define PPK_ASSERT_JOIN(lhs, rhs) PPK_ASSERT_JOIN_(lhs, rhs)
#define PPK_ASSERT_JOIN_(lhs, rhs) PPK_ASSERT_JOIN__(lhs, rhs)
#define PPK_ASSERT_JOIN__(lhs, rhs) lhs##rhs

#define PPK_ASSERT_FILE __FILE__
#define PPK_ASSERT_LINE __LINE__
#if defined(__GNUC__) || defined(__clang__)
#define PPK_ASSERT_FUNCTION __PRETTY_FUNCTION__
#elif defined(_MSC_VER)
#define PPK_ASSERT_FUNCTION __FUNCSIG__
#elif defined(__SUNPRO_CC)
#define PPK_ASSERT_FUNCTION __func__
#else
#define PPK_ASSERT_FUNCTION __FUNCTION__
#endif

#if defined(_MSC_VER)
#define PPK_ASSERT_ALWAYS_INLINE __forceinline
#elif defined(__GNUC__) || defined(__clang__)
#define PPK_ASSERT_ALWAYS_INLINE inline __attribute__((always_inline))
#else
#define PPK_ASSERT_ALWAYS_INLINE inline
#endif

#define PPK_ASSERT_NO_MACRO

#define PPK_ASSERT_APPLY_VA_ARGS(M, ...) PPK_ASSERT_APPLY_VA_ARGS_(M, (__VA_ARGS__))
#define PPK_ASSERT_APPLY_VA_ARGS_(M, args) M args

#define PPK_ASSERT_NARG(...) PPK_ASSERT_APPLY_VA_ARGS(PPK_ASSERT_NARG_, PPK_ASSERT_NO_MACRO, ##__VA_ARGS__,               \
                                                      32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, \
                                                      15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, PPK_ASSERT_NO_MACRO)
#define PPK_ASSERT_NARG_(_0, _1, _2, _3, _4, _5, _6, _7, _8,     \
                         _9, _10, _11, _12, _13, _14, _15, _16,  \
                         _17, _18, _19, _20, _21, _22, _23, _24, \
                         _25, _26, _27, _28, _29, _30, _31, _32, _33, ...) _33

#define PPK_ASSERT_HAS_ONE_ARG(...) PPK_ASSERT_APPLY_VA_ARGS(PPK_ASSERT_NARG_, PPK_ASSERT_NO_MACRO, ##__VA_ARGS__, \
                                                             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,    \
                                                             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, PPK_ASSERT_NO_MACRO)

#if defined(__GNUC__) || defined(__clang__)
#define PPK_ASSERT_LIKELY(arg) __builtin_expect(!!(arg), !0)
#define PPK_ASSERT_UNLIKELY(arg) __builtin_expect(!!(arg), 0)
#else
#define PPK_ASSERT_LIKELY(arg) (arg)
#define PPK_ASSERT_UNLIKELY(arg) (arg)
#endif

#define PPK_ASSERT_UNUSED(expression) (void)(true ? (void)0 : ((void)(expression)))

#if defined(_WIN32)
extern void __cdecl __debugbreak(void);
#define PPK_ASSERT_DEBUG_BREAK() __debugbreak()
#else
#if defined(__APPLE__)
#include <TargetConditionals.h>
#endif
#if defined(__clang__) && !TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#define PPK_ASSERT_DEBUG_BREAK() __builtin_debugtrap()
#elif defined(linux) || defined(__linux) || defined(__linux__) || defined(__APPLE__)
#include <signal.h>
#define PPK_ASSERT_DEBUG_BREAK() raise(SIGTRAP)
#elif defined(__GNUC__)
#define PPK_ASSERT_DEBUG_BREAK() __builtin_trap()
#else
#define PPK_ASSERT_DEBUG_BREAK() ((void)0)
#endif
#endif

#if (defined(__cplusplus) && (__cplusplus > 199711L)) || (defined(_MSC_FULL_VER) && (_MSC_FULL_VER >= 150020706))
#define PPK_ASSERT_CXX11
#endif

#if defined(PPK_ASSERT_CXX11)
#define PPK_ASSERT_NULLPTR nullptr
#else
#define PPK_ASSERT_NULLPTR 0
#endif

#define PPK_ASSERT_(level, ...) PPK_ASSERT_JOIN(PPK_ASSERT_, PPK_ASSERT_HAS_ONE_ARG(__VA_ARGS__)) \
(level, __VA_ARGS__)
#define PPK_ASSERT_0(level, ...) PPK_ASSERT_APPLY_VA_ARGS(PPK_ASSERT_2, level, __VA_ARGS__)
#define PPK_ASSERT_1(level, expression) PPK_ASSERT_2(level, expression, PPK_ASSERT_NULLPTR)

#if defined(_MSC_FULL_VER) && (_MSC_FULL_VER >= 140050215)

#if defined(PPK_ASSERT_DISABLE_IGNORE_LINE)

#define PPK_ASSERT_3(level, expression, ...)                                                                                                                                                                               \
    __pragma(warning(push))                                                                                                                                                                                                \
        __pragma(warning(disable : 4127)) do                                                                                                                                                                               \
    {                                                                                                                                                                                                                      \
        if (PPK_ASSERT_LIKELY(expression) || ppk::assert::implementation::ignoreAllAsserts())                                                                                                                              \
            ;                                                                                                                                                                                                              \
        else                                                                                                                                                                                                               \
        {                                                                                                                                                                                                                  \
            if (ppk::assert::implementation::handleAssert(PPK_ASSERT_FILE, PPK_ASSERT_LINE, PPK_ASSERT_FUNCTION, #expression, level, PPK_ASSERT_NULLPTR, __VA_ARGS__) == ppk::assert::implementation::AssertAction::Break) \
                PPK_ASSERT_DEBUG_BREAK();                                                                                                                                                                                  \
        }                                                                                                                                                                                                                  \
    }                                                                                                                                                                                                                      \
    while (false)                                                                                                                                                                                                          \
    __pragma(warning(pop))

#else

#define PPK_ASSERT_3(level, expression, ...)                                                                                                                                                                     \
    __pragma(warning(push))                                                                                                                                                                                      \
        __pragma(warning(disable : 4127)) do                                                                                                                                                                     \
    {                                                                                                                                                                                                            \
        static bool _ignore = false;                                                                                                                                                                             \
        if (PPK_ASSERT_LIKELY(expression) || _ignore || ppk::assert::implementation::ignoreAllAsserts())                                                                                                         \
            ;                                                                                                                                                                                                    \
        else                                                                                                                                                                                                     \
        {                                                                                                                                                                                                        \
            if (ppk::assert::implementation::handleAssert(PPK_ASSERT_FILE, PPK_ASSERT_LINE, PPK_ASSERT_FUNCTION, #expression, level, &_ignore, __VA_ARGS__) == ppk::assert::implementation::AssertAction::Break) \
                PPK_ASSERT_DEBUG_BREAK();                                                                                                                                                                        \
        }                                                                                                                                                                                                        \
    }                                                                                                                                                                                                            \
    while (false)                                                                                                                                                                                                \
    __pragma(warning(pop))

#endif

#else

#if (defined(__GNUC__) && ((__GNUC__ * 1000 + __GNUC_MINOR__ * 100) >= 4600)) || defined(__clang__)
#define _pragma(x) _Pragma(#x)
#define _PPK_ASSERT_WFORMAT_AS_ERROR_BEGIN \
    _pragma(GCC diagnostic push)           \
        _pragma(GCC diagnostic error "-Wformat")
#define _PPK_ASSERT_WFORMAT_AS_ERROR_END \
    _pragma(GCC diagnostic pop)
#else
#define _PPK_ASSERT_WFORMAT_AS_ERROR_BEGIN
#define _PPK_ASSERT_WFORMAT_AS_ERROR_END
#endif

#if defined(PPK_ASSERT_DISABLE_IGNORE_LINE)

#define PPK_ASSERT_3(level, expression, ...)                                                                                                                                                                               \
    do                                                                                                                                                                                                                     \
    {                                                                                                                                                                                                                      \
        if (PPK_ASSERT_LIKELY(expression) || ppk::assert::implementation::ignoreAllAsserts())                                                                                                                              \
            ;                                                                                                                                                                                                              \
        else                                                                                                                                                                                                               \
        {                                                                                                                                                                                                                  \
            _PPK_ASSERT_WFORMAT_AS_ERROR_BEGIN                                                                                                                                                                             \
            if (ppk::assert::implementation::handleAssert(PPK_ASSERT_FILE, PPK_ASSERT_LINE, PPK_ASSERT_FUNCTION, #expression, level, PPK_ASSERT_NULLPTR, __VA_ARGS__) == ppk::assert::implementation::AssertAction::Break) \
                PPK_ASSERT_DEBUG_BREAK();                                                                                                                                                                                  \
            _PPK_ASSERT_WFORMAT_AS_ERROR_END                                                                                                                                                                               \
        }                                                                                                                                                                                                                  \
    } while (false)

#else

#define PPK_ASSERT_3(level, expression, ...)                                                                                                                                                                     \
    do                                                                                                                                                                                                           \
    {                                                                                                                                                                                                            \
        static bool _ignore = false;                                                                                                                                                                             \
        if (PPK_ASSERT_LIKELY(expression) || _ignore || ppk::assert::implementation::ignoreAllAsserts())                                                                                                         \
            ;                                                                                                                                                                                                    \
        else                                                                                                                                                                                                     \
        {                                                                                                                                                                                                        \
            _PPK_ASSERT_WFORMAT_AS_ERROR_BEGIN                                                                                                                                                                   \
            if (ppk::assert::implementation::handleAssert(PPK_ASSERT_FILE, PPK_ASSERT_LINE, PPK_ASSERT_FUNCTION, #expression, level, &_ignore, __VA_ARGS__) == ppk::assert::implementation::AssertAction::Break) \
                PPK_ASSERT_DEBUG_BREAK();                                                                                                                                                                        \
            _PPK_ASSERT_WFORMAT_AS_ERROR_END                                                                                                                                                                     \
        }                                                                                                                                                                                                        \
    } while (false)

#endif

#endif

#define PPK_ASSERT_USED_(...) PPK_ASSERT_USED_0(PPK_ASSERT_NARG(__VA_ARGS__), __VA_ARGS__)
#define PPK_ASSERT_USED_0(N, ...) PPK_ASSERT_JOIN(PPK_ASSERT_USED_, N) \
(__VA_ARGS__)

#define PPK_STATIC_ASSERT(...) PPK_ASSERT_APPLY_VA_ARGS(PPK_ASSERT_JOIN(PPK_STATIC_ASSERT_, PPK_ASSERT_HAS_ONE_ARG(__VA_ARGS__)), __VA_ARGS__)
#if defined(PPK_ASSERT_CXX11)
#define PPK_STATIC_ASSERT_0(expression, message) static_assert(expression, message)
#else
#define PPK_STATIC_ASSERT_0(expression, message)                                                                                                          \
    struct PPK_ASSERT_JOIN(_ppk_static_assertion_at_line_, PPK_ASSERT_LINE)                                                                               \
    {                                                                                                                                                     \
        ppk::assert::implementation::StaticAssertion<static_cast<bool>((expression))> PPK_ASSERT_JOIN(STATIC_ASSERTION_FAILED_AT_LINE_, PPK_ASSERT_LINE); \
    };                                                                                                                                                    \
    typedef ppk::assert::implementation::StaticAssertionTest<sizeof(PPK_ASSERT_JOIN(_ppk_static_assertion_at_line_, PPK_ASSERT_LINE))> PPK_ASSERT_JOIN(_ppk_static_assertion_test_at_line_, PPK_ASSERT_LINE)
// note that we wrap the non existing type inside a struct to avoid warning
// messages about unused variables when static assertions are used at function
// scope
// the use of sizeof makes sure the assertion error is not ignored by SFINAE
#endif
#define PPK_STATIC_ASSERT_1(expression) PPK_STATIC_ASSERT_0(expression, #expression)

#if !defined(PPK_ASSERT_CXX11)
namespace ppk
{
namespace assert
{
namespace implementation
{

template <bool>
struct StaticAssertion;

template <>
struct StaticAssertion<true>
{
}; // StaticAssertion<true>

template <int i>
struct StaticAssertionTest
{
}; // StaticAssertionTest<int>

} // namespace implementation
} // namespace assert
} // namespace ppk
#endif

#if !defined(PPK_ASSERT_DISABLE_STL)
#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4548)
#pragma warning(disable : 4710)
#endif
#include <stdexcept>
#if defined(_MSC_VER)
#pragma warning(pop)
#endif
#endif

#if !defined(PPK_ASSERT_EXCEPTION_MESSAGE_BUFFER_SIZE)
#define PPK_ASSERT_EXCEPTION_MESSAGE_BUFFER_SIZE 1024
#endif

#if defined(PPK_ASSERT_CXX11) && !defined(_MSC_VER)
#define PPK_ASSERT_EXCEPTION_NO_THROW noexcept(true)
#else
#define PPK_ASSERT_EXCEPTION_NO_THROW throw()
#endif

#if defined(PPK_ASSERT_CXX11)
#include <utility>
#endif

namespace ppk
{
namespace assert
{

#if !defined(PPK_ASSERT_DISABLE_STL)
class AssertionException : public std::exception
#else
class AssertionException
#endif
{
  public:
    explicit AssertionException(const char *file,
                                int line,
                                const char *function,
                                const char *expression,
                                const char *message);

    AssertionException(const AssertionException &rhs);

    virtual ~AssertionException() PPK_ASSERT_EXCEPTION_NO_THROW;

    AssertionException &operator=(const AssertionException &rhs);

    virtual const char *what() const PPK_ASSERT_EXCEPTION_NO_THROW;

    const char *file() const;
    int line() const;
    const char *function() const;
    const char *expression() const;

  private:
    const char *_file;
    int _line;
    const char *_function;
    const char *_expression;

    enum
    {
        request = PPK_ASSERT_EXCEPTION_MESSAGE_BUFFER_SIZE,
        size = request > sizeof(char *) ? request : sizeof(char *) + 1
    };

    union {
        char _stack[size];
        char *_heap;
    };

    PPK_STATIC_ASSERT(size > sizeof(char *), "invalid_size");
}; // AssertionException

PPK_ASSERT_ALWAYS_INLINE const char *AssertionException::file() const
{
    return _file;
}

PPK_ASSERT_ALWAYS_INLINE int AssertionException::line() const
{
    return _line;
}

PPK_ASSERT_ALWAYS_INLINE const char *AssertionException::function() const
{
    return _function;
}

PPK_ASSERT_ALWAYS_INLINE const char *AssertionException::expression() const
{
    return _expression;
}

namespace implementation
{

#if defined(_MSC_VER) && !defined(_CPPUNWIND)
#if !defined(PPK_ASSERT_DISABLE_EXCEPTIONS)
#define PPK_ASSERT_DISABLE_EXCEPTIONS
#endif
#endif

#if !defined(PPK_ASSERT_DISABLE_EXCEPTIONS)

template <typename E>
inline void throwException(const E &e)
{
    throw e;
}

#else

// user defined, the behavior is undefined if the function returns
void throwException(const ppk::assert::AssertionException &e);

#endif

namespace AssertLevel
{

enum AssertLevel
{
    Warning = 32,
    Debug = 64,
    Error = 128,
    Fatal = 256

}; // AssertLevel

} // namespace AssertLevel

namespace AssertAction
{

enum AssertAction
{
    None,
    Abort,
    Break,
    Ignore,
#if !defined(PPK_ASSERT_DISABLE_IGNORE_LINE)
    IgnoreLine,
#endif
    IgnoreAll,
    Throw

}; // AssertAction

} // namespace AssertAction

#if !defined(PPK_ASSERT_CALL)
#define PPK_ASSERT_CALL
#endif

typedef AssertAction::AssertAction(PPK_ASSERT_CALL *AssertHandler)(const char *file,
                                                                   int line,
                                                                   const char *function,
                                                                   const char *expression,
                                                                   int level,
                                                                   const char *message);

#if defined(__GNUC__) || defined(__clang__)
#define PPK_ASSERT_HANDLE_ASSERT_FORMAT __attribute__((format(printf, 7, 8)))
#else
#define PPK_ASSERT_HANDLE_ASSERT_FORMAT
#endif

#if !defined(PPK_ASSERT_FUNCSPEC)
#define PPK_ASSERT_FUNCSPEC
#endif

PPK_ASSERT_FUNCSPEC
AssertAction::AssertAction PPK_ASSERT_CALL handleAssert(const char *file,
                                                        int line,
                                                        const char *function,
                                                        const char *expression,
                                                        int level,
                                                        bool *ignoreLine,
                                                        const char *message, ...) PPK_ASSERT_HANDLE_ASSERT_FORMAT;

PPK_ASSERT_FUNCSPEC
AssertHandler PPK_ASSERT_CALL setAssertHandler(AssertHandler handler);

PPK_ASSERT_FUNCSPEC
void PPK_ASSERT_CALL ignoreAllAsserts(bool value);

PPK_ASSERT_FUNCSPEC
bool PPK_ASSERT_CALL ignoreAllAsserts();

#if defined(PPK_ASSERT_CXX11)

template <int level, typename T>
class AssertUsedWrapper
{
  public:
    AssertUsedWrapper(T &&t);
    ~AssertUsedWrapper() PPK_ASSERT_EXCEPTION_NO_THROW;

    operator T();

  private:
    const AssertUsedWrapper &operator=(const AssertUsedWrapper &); // not implemented on purpose (and only VS2013 supports deleted functions)

    T t;
    mutable bool used;

}; // AssertUsedWrapper<int, T>

template <int level, typename T>
inline AssertUsedWrapper<level, T>::AssertUsedWrapper(T &&_t)
    : t(std::forward<T>(_t)), used(false)
{
}

template <int level, typename T>
inline AssertUsedWrapper<level, T>::operator T()
{
    used = true;
    return std::move(t);
}

template <int level, typename T>
inline AssertUsedWrapper<level, T>::~AssertUsedWrapper() PPK_ASSERT_EXCEPTION_NO_THROW
{
    PPK_ASSERT_3(level, used, "unused value");
}

#else

template <int level, typename T>
class AssertUsedWrapper
{
  public:
    AssertUsedWrapper(const T &t);
    AssertUsedWrapper(const AssertUsedWrapper &rhs);
    ~AssertUsedWrapper() PPK_ASSERT_EXCEPTION_NO_THROW;

    operator T() const;

  private:
    const AssertUsedWrapper &operator=(const AssertUsedWrapper &); // not implemented on purpose

    T t;
    mutable bool used;

}; // AssertUsedWrapper<int, T>

template <int level, typename T>
PPK_ASSERT_ALWAYS_INLINE AssertUsedWrapper<level, T>::AssertUsedWrapper(const T &_t)
    : t(_t), used(false)
{
}

template <int level, typename T>
PPK_ASSERT_ALWAYS_INLINE AssertUsedWrapper<level, T>::AssertUsedWrapper(const AssertUsedWrapper &rhs)
    : t(rhs.t), used(rhs.used)
{
}

// /!\ GCC is not so happy if we inline that destructor
template <int level, typename T>
AssertUsedWrapper<level, T>::~AssertUsedWrapper() PPK_ASSERT_EXCEPTION_NO_THROW
{
    PPK_ASSERT_3(level, used, "unused value");
}

template <int level, typename T>
PPK_ASSERT_ALWAYS_INLINE AssertUsedWrapper<level, T>::operator T() const
{
    used = true;
    return t;
}

#endif

} // namespace implementation

} // namespace assert
} // namespace ppk

#endif

#undef PPK_ASSERT_2
#undef PPK_ASSERT_USED_1
#undef PPK_ASSERT_USED_2

#if defined(_MSC_VER) && defined(_PREFAST_)

#define PPK_ASSERT_2(level, expression, ...) __analysis_assume(!!(expression))
#define PPK_ASSERT_USED_1(type) type
#define PPK_ASSERT_USED_2(level, type) type

#elif defined(__clang__) && defined(__clang_analyzer__)

void its_going_to_be_ok(bool expression) __attribute__((analyzer_noreturn));
#define PPK_ASSERT_2(level, expression, ...) its_going_to_be_ok(!!(expression))
#define PPK_ASSERT_USED_1(type) type
#define PPK_ASSERT_USED_2(level, type) type

#else

#if PPK_ASSERT_ENABLED

#define PPK_ASSERT_2(level, expression, ...) PPK_ASSERT_3(level, expression, __VA_ARGS__)
#define PPK_ASSERT_USED_1(type) ppk::assert::implementation::AssertUsedWrapper<ppk::assert::implementation::AssertLevel::PPK_ASSERT_DEFAULT_LEVEL, type>
#define PPK_ASSERT_USED_2(level, type) ppk::assert::implementation::AssertUsedWrapper<level, type>

#else

#define PPK_ASSERT_2(level, expression, ...) PPK_ASSERT_UNUSED(expression)
#define PPK_ASSERT_USED_1(type) type
#define PPK_ASSERT_USED_2(level, type) type

#endif

#endif

#if (defined(__GNUC__) && ((__GNUC__ * 1000 + __GNUC_MINOR__ * 100) >= 4600)) || defined(__clang__)
#pragma GCC diagnostic pop
#endif

// PPK ASSERTS CPP FILE
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
// see README.md for usage instructions.
// (‑●‑●)> released under the WTFPL v2 license, by Gregory Pakosz (@gpakosz)

#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#endif

//#include <ppk_assert.h> everything is here

#include <cstdio> // fprintf() and vsnprintf()
#include <cstring>
#include <cstdarg> // va_start() and va_end()
#include <cstdlib> // abort()

#if defined(__APPLE__)
#include <TargetConditionals.h>
#endif

#if defined(__ANDROID__) || defined(ANDROID)
#include <android/log.h>
#if !defined(PPK_ASSERT_LOG_TAG)
#define PPK_ASSERT_LOG_TAG "PPK_ASSERT"
#endif
#endif

//#define PPK_ASSERT_LOG_FILE "/tmp/assert.txt"
//#define PPK_ASSERT_LOG_FILE_TRUNCATE

// malloc and free are only used by AssertionException implemented in terms of
// short string optimization.
// However, no memory allocation happens if
// PPK_ASSERT_EXCEPTION_MESSAGE_BUFFER_SIZE == PPK_ASSERT_MESSAGE_BUFFER_SIZE
// which is the default.
#if !defined(PPK_ASSERT_MALLOC)
#define PPK_ASSERT_MALLOC(size) malloc(size)
#endif

#if !defined(PPK_ASSERT_FREE)
#define PPK_ASSERT_FREE(p) free(p)
#endif

#if !defined(PPK_ASSERT_MESSAGE_BUFFER_SIZE)
#define PPK_ASSERT_MESSAGE_BUFFER_SIZE PPK_ASSERT_EXCEPTION_MESSAGE_BUFFER_SIZE
#endif

#if !defined(PPK_ASSERT_ABORT)
#define PPK_ASSERT_ABORT abort
#endif

namespace
{

namespace AssertLevel = ppk::assert::implementation::AssertLevel;
namespace AssertAction = ppk::assert::implementation::AssertAction;

typedef int (*printHandler)(FILE *out, int, const char *format, ...);

#if defined(PPK_ASSERT_LOG_FILE) && defined(PPK_ASSERT_LOG_FILE_TRUNCATE)
struct LogFileTruncate
{
    LogFileTruncate()
    {
        if (FILE *f = fopen(PPK_ASSERT_LOG_FILE, "w"))
            fclose(f);
    }
};

static LogFileTruncate truncate;
#endif

int print(FILE *out, int level, const char *format, ...)
{
    va_list args;
    int count;

    va_start(args, format);
    count = vfprintf(out, format, args);
    fflush(out);
    va_end(args);

#if defined(PPK_ASSERT_LOG_FILE)
    struct Local
    {
        static void log(const char *_format, va_list _args)
        {
            if (FILE *f = fopen(PPK_ASSERT_LOG_FILE, "a"))
            {
                vfprintf(f, _format, _args);
                fclose(f);
            }
        }
    };

    va_start(args, format);
    Local::log(format, args);
    va_end(args);
#endif

#if defined(_WIN32)
    char buffer[PPK_ASSERT_MESSAGE_BUFFER_SIZE];
    va_start(args, format);
    vsnprintf(buffer, PPK_ASSERT_MESSAGE_BUFFER_SIZE, format, args);
    ::OutputDebugStringA(buffer);
    va_end(args);
#endif

#if defined(__ANDROID__) || defined(ANDROID)
    int priority = ANDROID_LOG_VERBOSE;

    if (level >= AssertLevel::Debug)
        priority = ANDROID_LOG_DEBUG;
    else if (level >= AssertLevel::Warning)
        priority = ANDROID_LOG_WARN;
    else if (level >= AssertLevel::Error)
        priority = ANDROID_LOG_ERROR;
    else if (level >= AssertLevel::Fatal)
        priority = ANDROID_LOG_FATAL;

    va_start(args, format);
    __android_log_vprint(priority, PPK_ASSERT_LOG_TAG, format, args);
    va_start(args, format);
#else
    PPK_ASSERT_UNUSED(level);
#endif

    return count;
}

int formatLevel(int level, const char *expression, FILE *out, printHandler print)
{
    const char *levelstr = 0;

    switch (level)
    {
    case AssertLevel::Debug:
        levelstr = "DEBUG";
        break;
    case AssertLevel::Warning:
        levelstr = "WARNING";
        break;
    case AssertLevel::Error:
        levelstr = "ERROR";
        break;
    case AssertLevel::Fatal:
        levelstr = "FATAL";
        break;

    default:
        break;
    }

    if (levelstr)
        return print(out, level, "Assertion '%s' failed (%s)\n", expression, levelstr);
    else
        return print(out, level, "Assertion '%s' failed (level = %d)\n", expression, level);
}

AssertAction::AssertAction PPK_ASSERT_CALL _defaultHandler(const char *file,
                                                           int line,
                                                           const char *function,
                                                           const char *expression,
                                                           int level,
                                                           const char *message)
{
#if defined(_WIN32)
    if (::GetConsoleWindow() == NULL)
    {
        if (::AllocConsole())
        {
            (void)freopen("CONIN$", "r", stdin);
            (void)freopen("CONOUT$", "w", stdout);
            (void)freopen("CONOUT$", "w", stderr);

            SetFocus(::GetConsoleWindow());
        }
    }
#endif

    formatLevel(level, expression, stderr, reinterpret_cast<printHandler>(print));
    print(stderr, level, "  in file %s, line %d\n  function: %s\n", file, line, function);

    if (message)
        print(stderr, level, "  with message: %s\n\n", message);

    if (level < AssertLevel::Debug)
    {
        return AssertAction::None;
    }
    else if (AssertLevel::Debug <= level && level < AssertLevel::Error)
    {
#if (!TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR) && (!defined(__ANDROID__) && !defined(ANDROID)) || defined(PPK_ASSERT_DEFAULT_HANDLER_STDIN)
        for (;;)
        {
#if defined(PPK_ASSERT_DISABLE_IGNORE_LINE)
            fprintf(stderr, "Press (I)gnore / Ignore (A)ll / (D)ebug / A(b)ort: ");
#else
            fprintf(stderr, "Press (I)gnore / Ignore (F)orever / Ignore (A)ll / (D)ebug / A(b)ort: ");
#endif
            fflush(stderr);

            char buffer[256];
            if (!fgets(buffer, sizeof(buffer), stdin))
            {
                clearerr(stdin);
                fprintf(stderr, "\n");
                fflush(stderr);
                continue;
            }

            // we eventually skip the leading spaces but that's it
            char input[2] = {'b', 0};
            if (sscanf(buffer, " %1[a-zA-Z] ", input) != 1)
                continue;

            switch (*input)
            {
            case 'b':
            case 'B':
                return AssertAction::Abort;

            case 'd':
            case 'D':
                return AssertAction::Break;

            case 'i':
            case 'I':
                return AssertAction::Ignore;

#if !defined(PPK_ASSERT_DISABLE_IGNORE_LINE)
            case 'f':
            case 'F':
                return AssertAction::IgnoreLine;
#endif

            case 'a':
            case 'A':
                return AssertAction::IgnoreAll;

            default:
                break;
            }
        }
#else
        return AssertAction::Break;
#endif
    }
    else if (AssertLevel::Error <= level && level < AssertLevel::Fatal)
    {
        return AssertAction::Throw;
    }

    return AssertAction::Abort;
}

void _throw(const char *file,
            int line,
            const char *function,
            const char *expression,
            const char *message)
{
    using ppk::assert::implementation::throwException;
    throwException(ppk::assert::AssertionException(file, line, function, expression, message));
}
} // namespace

namespace ppk
{
namespace assert
{

AssertionException::AssertionException(const char *file,
                                       int line,
                                       const char *function,
                                       const char *expression,
                                       const char *message)
    : _file(file), _line(line), _function(function), _expression(expression), _heap(PPK_ASSERT_NULLPTR)
{
    if (!message)
    {
        strncpy(_stack, "", size);
        return;
    }

    size_t length = strlen(message);

    if (length < size) // message is short enough for the stack buffer
    {
        strncpy(_stack, message, length);
        strncpy(_stack + length, "", size - length); // pad with 0
    }
    else // allocate storage on the heap
    {
        _heap = static_cast<char *>(PPK_ASSERT_MALLOC(sizeof(char) * (length + 1)));

        if (!_heap) // allocation failed
        {
            strncpy(_stack, message, size - 1); // stack fallback, truncate :/
            _stack[size - 1] = 0;
        }
        else
        {
            strncpy(_heap, message, length); // copy the message
            _heap[length] = 0;
            _stack[size - 1] = 1; // mark the stack
        }
    }
}

AssertionException::AssertionException(const AssertionException &rhs)
    : _file(rhs._file), _line(rhs._line), _function(rhs._function), _expression(rhs._expression)
{
    const char *message = rhs.what();
    size_t length = strlen(message);

    if (length < size) // message is short enough for the stack buffer
    {
        strncpy(_stack, message, size); // pad with 0
    }
    else // allocate storage on the heap
    {
        _heap = static_cast<char *>(PPK_ASSERT_MALLOC(sizeof(char) * (length + 1)));

        if (!_heap) // allocation failed
        {
            strncpy(_stack, message, size - 1); // stack fallback, truncate :/
            _stack[size - 1] = 0;
        }
        else
        {
            strncpy(_heap, message, length); // copy the message
            _heap[length] = 0;
            _stack[size - 1] = 1; // mark the stack
        }
    }
}

AssertionException::~AssertionException() PPK_ASSERT_EXCEPTION_NO_THROW
{
    if (_stack[size - 1])
        PPK_ASSERT_FREE(_heap);

    _heap = PPK_ASSERT_NULLPTR; // in case the exception object is destroyed twice
    _stack[size - 1] = 0;
}

AssertionException &AssertionException::operator=(const AssertionException &rhs)
{
    if (&rhs == this)
        return *this;

    const char *message = rhs.what();
    size_t length = strlen(message);

    if (length < size) // message is short enough for the stack buffer
    {
        if (_stack[size - 1])
            PPK_ASSERT_FREE(_heap);

        strncpy(_stack, message, size);
    }
    else // allocate storage on the heap
    {
        if (_stack[size - 1])
        {
            size_t _length = strlen(_heap);

            if (length <= _length)
            {
                strncpy(_heap, message, _length); // copy the message, pad with 0
                return *this;
            }
            else
            {
                PPK_ASSERT_FREE(_heap);
            }
        }

        _heap = static_cast<char *>(PPK_ASSERT_MALLOC(sizeof(char) * (length + 1)));

        if (!_heap) // allocation failed
        {
            strncpy(_stack, message, size - 1); // stack fallback, truncate :/
            _stack[size - 1] = 0;
        }
        else
        {
            strncpy(_heap, message, length); // copy the message
            _heap[length] = 0;
            _stack[size - 1] = 1; // mark the stack
        }
    }

    _file = rhs._file;
    _line = rhs._line;
    _function = rhs._function;
    _expression = rhs._expression;

    return *this;
}

const char *AssertionException::what() const PPK_ASSERT_EXCEPTION_NO_THROW
{
    return _stack[size - 1] ? _heap : _stack;
}

namespace implementation
{

namespace
{
bool _ignoreAll = false;
}

void PPK_ASSERT_CALL ignoreAllAsserts(bool value)
{
    _ignoreAll = value;
}

bool PPK_ASSERT_CALL ignoreAllAsserts()
{
    return _ignoreAll;
}

namespace
{
AssertHandler _handler = _defaultHandler;
}

AssertHandler PPK_ASSERT_CALL setAssertHandler(AssertHandler handler)
{
    AssertHandler previous = _handler;

    _handler = handler ? handler : _defaultHandler;

    return previous;
}

AssertAction::AssertAction PPK_ASSERT_CALL handleAssert(const char *file,
                                                        int line,
                                                        const char *function,
                                                        const char *expression,
                                                        int level,
                                                        bool *ignoreLine,
                                                        const char *message, ...)
{
    char message_[PPK_ASSERT_MESSAGE_BUFFER_SIZE] = {0};
    const char *file_;

    if (message)
    {
        va_list args;
        va_start(args, message);
        vsnprintf(message_, PPK_ASSERT_MESSAGE_BUFFER_SIZE, message, args);
        va_end(args);

        message = message_;
    }

#if defined(_WIN32)
    file_ = strrchr(file, '\\');
#else
    file_ = strrchr(file, '/');
#endif // #if defined(_WIN32)

    file = file_ ? file_ + 1 : file;
    AssertAction::AssertAction action = _handler(file, line, function, expression, level, message);

    switch (action)
    {
    case AssertAction::Abort:
        PPK_ASSERT_ABORT();

#if !defined(PPK_ASSERT_DISABLE_IGNORE_LINE)
    case AssertAction::IgnoreLine:
        *ignoreLine = true;
        break;
#else
        PPK_ASSERT_UNUSED(ignoreLine);
#endif

    case AssertAction::IgnoreAll:
        ignoreAllAsserts(true);
        break;

    case AssertAction::Throw:
        _throw(file, line, function, expression, message);
        break;

    case AssertAction::Ignore:
    case AssertAction::Break:
    case AssertAction::None:
    default:
        return action;
    }

    return AssertAction::None;
}

} // namespace implementation
} // namespace assert
} // namespace ppk

// COPYRIGHT END
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

// normal student stuff begins here, thanks




































#ifdef LOUIS_DEBUG
#define LOG(X) std::cout << X
#else
#define LOG(X)
#endif

namespace lou
{

    const char *bool_str(bool b)
    {
        static const char *BOOL_STR[] = {"False", "True"};
        return b ? BOOL_STR[1] : BOOL_STR[0];
    }

    std::string tab_str(size_t tab)
    {
        std::stringstream os;

        for (size_t i = 0; i < tab; i++)
        {
            os << " - ";
        }
        return os.str();
    }

    std::string title_str(const std::string &str)
    {
        std::stringstream os;
        os << str << std::endl;
        os << "==============================\n";
        return os.str();
    }

    std::string vec_str(const std::vector<int> &vec)
    {
        std::stringstream os;
        for (size_t i = 0; i < vec.size(); i++)
        {
            os << (vec[i]);
            if (i + 1 < vec.size())
            {
                os << (",");
            }
        }
        os << ("\n");
        return os.str();
    }

    template <class T>
    bool in_range_inc(T v, T min, T max)
    {
        return min <= v && v <= max;
    }

    template <class T, size_t N>
    constexpr size_t arr_size(T (&)[N]) { return N; }

} // namespace lou





















std::ostream& operator<< (std::ostream& os, Point const& p) {
	os << "(" << p.x << " , " << p.y << ") ";
	return os;
}

std::istream& operator>> (std::istream& os, Point & p) {
	os >> p.x >> p.y;
	return os;
}

////////////////////////////////////////////////////////////////////////////////
float closestPair_aux ( std::vector< Point > const& points ) {
	int size = points.size();

	if (size==0) throw "zero size subset";
	if (size==1) return std::numeric_limits<float>::max();

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
float closestPair ( std::vector< Point > const& points ) {
	int size = points.size();

	if (size==0) throw "zero size subset";
	if (size==1) return std::numeric_limits<float>::max();

	return closestPair_aux(points);
}

